<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Research problem: measuring the safety of the Tor network</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Research problem: measuring the safety of the Tor network</h2>
<p class="meta">06 Feb 2011</p>

<div class="post">
<p>We need a better understanding of how much anonymity the Tor network provides against a partial network adversary who observes and/or operates some of the network. Specifically, we want to understand the chances that this class of adversary can observe a user&#39;s traffic coming into the network and also the corresponding traffic exiting the network.</p>

<p>Most of our graphs historically have looked at the total number of relays over time. But this simple scalar doesn&#39;t take into account relay capacity, exit policies, geographic or network location, etc.</p>

<p>One concrete problem from this bad metric is that when many new relays show up (e.g. due to a <a href="https://blog.torproject.org/files/relays-diff-2011-01-28.png">political event</a>), we really don&#39;t have any insight about how the diversity of the new relays compares to the rest of the network. The simplistic metric also gets misused in academic research papers to produce misleading results like &quot;I can sign up 2% of the Tor relays and capture 50% of the paths,&quot; when what they mean is &quot;I can provide half the capacity in the Tor network and capture half the paths.&quot;</p>

<p>There are four parts to this research problem. First, we need a broad array of metrics that reflect various instances of our partial network adversary. Second, we need to understand for each metric how stable it is (sensitivity analysis) and what changes in the Tor network would efficiently improve (or harm) it. Third, we should make the calculations as realistic as possible based on actual Tor client behavior. Last, we need a better infrastructure for comparing the safety impact from alternate design scenarios, for example different path selection or route balancing algorithms.</p>

<p>We&#39;d love to <a href="https://www.torproject.org/research">work with you</a> to help answer these questions.</p>

<p><strong>Part one: new metrics</strong></p>

<p>The <a href="http://metrics.torproject.org/network.html">network graphs</a> on our metrics site show the number of relays and advertised capacity over time. More importantly, we&#39;ve archived all the <a href="http://metrics.torproject.org/data.html#relaydesc">relay descriptors and network consensus documents</a> going back to 2004. So now we can ask some questions to better capture the safety of the network over time.</p>

<p>What is the entropy of path selection over time? When we add really fast relays, the capacity of the network goes up but the safety of the network can go down because it gets less balanced. We can start with a simple approximation of a Tor client&#39;s path selection behavior — e.g., choose the first hop randomly weighted by bandwidth, and choose the final hop randomly weighted by bandwidth from among the nodes that can exit to port 80. I&#39;m particularly interested in the entropy of the first hop and the third hop, since those are the key points for the anonymity Tor provides against a partial network adversary. Calculating entropy on the probability distribution of possible first hops is easy (as is last hops), and it&#39;s worth looking at these individual components so we have better intuition about where our anonymity comes from. Then we&#39;d also want to look at the unified metric (probability distribution of first cross last), which in the simple approximation is the sum of the two entropies.</p>

<p>Then we should do the same entropy calculations, but lumping relays together based on various shared characteristics. For example, country diversity: what&#39;s the entropy of path selection over time when you treat all relays in Germany as one big relay, all relays in France as one big relay, etc? How about <a href="http://en.wikipedia.org/wiki/Autonomous_system_%28Internet%29">autonomous system (AS)</a> based diversity, where all the relays in AT&amp;T&#39;s network are lumped together?</p>

<p>Another angle to evaluate is what expected fraction of paths have the first and last hop in the same country, or the same AS, or the same city, or the same continent. What expected fraction of paths cross at least one ocean? How about looking at the AS-level paths <em>between</em> relays, like the research from <a href="http://freehaven.net/anonbib/#feamster:wpes2004">Feamster</a> or <a href="http://freehaven.net/anonbib/#DBLP:conf/ccs/EdmanS09">Edman</a>? What user locations are more safe or less safe in the above metrics?</p>

<p>It&#39;s plausible to imagine we can also gain some intuition when looking at the <em>possible diversity</em> rather than the entropy. How many ASes or countries total are represented in the network at a given time?</p>

<p>The goal here isn&#39;t to come up with the one true metric for summarizing Tor&#39;s safety. Rather, we need to recognize that there are many threat models to consider at once, so we need many different views into what types of safety the network can offer.</p>

<p><strong>Part two: how robust are these metrics?</strong></p>

<p>For each of the above metrics, how stable are they when you add or remove a few relays? Are certain relays critical to the safety of the Tor network? One way to look at this question is to graph the fall-off of safety as you remove relays from the network, in one case choosing victims randomly and in another choosing them to optimally harm the network. In the latter case, I expect it will look pretty dire. Then look at the same scenario, but now look at the safety of the network as you remove <em>capacity</em> from the network. For example, consider that the adversary&#39;s cost of removing a relay is equal to its capacity. Then explore these attacks again, but rather than looking at attacking individual relays look at attacks on ASes, countries, or continents.</p>

<p>Then look at the robustness over time: is an adversary that can knock out X% of the capacity in the network (for various values of X) getting more or less influential as the network grows? How about an adversary who can knock out an absolute capacity of K bytes for various values of K?</p>

<p>From the other direction, are there certain geographic or network locations where adding relays with a given capacity will most influence your safety metrics? This influence could be positive (&quot;where should I put my relay to best help the Tor network?&quot;) or negative (&quot;where should I put my relay to best attack users?&quot;).</p>

<p>Is there any relationship between the rate of new relay arrival (e.g. during political events or after popular conferences) and the level of diversity of these relays?</p>

<p><strong>Part three: how good was the simple approximation?</strong></p>

<p>While the above calculations assume a simplified path selection model, the <a href="https://git.torproject.org/tor/doc/spec/path-spec.txt">reality</a> is more complex. Clients avoid using more than one relay from a given &quot;/16&quot; network or <a href="https://www.torproject.org/docs/faq#ManyRelays">family</a> in their paths. They only use relays with the <a href="https://www.torproject.org/docs/faq#EntryGuards">Guard</a> <a href="https://git.torproject.org/tor/doc/spec/dir-spec.txt">flag</a> for their first hop. They read weighting parameters from the consensus and use them to avoid Guard-flagged nodes for positions other than the first hop and avoid Exit-flagged nodes for positions other than the last hop, in proportion to how much capacity is available in each category. They track how long it takes them to build circuits, and preemptively discard the slowest 20% of their paths.</p>

<p>Accounting for all of these behaviors (especially the last one) will be hard, and you&#39;ll want to work closely with the Tor developers to make sure you&#39;re moving in the right direction. But even if you don&#39;t handle all of them, having a more realistic sense of how clients behave should allow you to better capture the safety in the live Tor network. If you want extra feedback, you can compare your path selection predictions with <a href="https://metrics.torproject.org/data/moria-50kb.extradata">paths that Tor chooses in practice</a> (full data <a href="https://metrics.torproject.org/data.html#performance">here</a>).</p>

<p>We should rerun the above experiments with the more accurate client behavior, and see if any of the results change substantially, and if so, why. These changes are great places to recognize and reconsider tradeoffs between what we should do for maximum safety according to these metrics vs what we should do to optimize other metrics like performance and vulnerability to other attacks. Some of these differences are intentional; for example, we don&#39;t give the Guard flag to every relay because we want to reduce the churn of entry guards. But others are due to design mistakes; for example, we are probably not giving the Guard flag out as broadly as we should, and I imagine that really hurts the network&#39;s safety.</p>

<p>How far away is the &quot;optimal&quot; approximation curve you produced in part one from the &quot;in practice&quot; curve here? How does the divergence from the optimal look over time? That is, were we at 50% of optimal back in 2007, but now we&#39;re only at 20%?</p>

<p><strong>Part four: consider alternate designs</strong></p>

<p>Once we&#39;ve looked at how safe the Tor network has been over time for our broad array of metrics, we should do experiments to evaluate alternate network designs.</p>

<p>Examples include:</p>

<p>A) If we give out Guard flags according to some other algorithm, how much safety can we get back?</p>

<p>B) In Tor 0.2.1.x we started load balancing based on <a href="https://svn.torproject.org/svn/torflow/trunk/NetworkScanners/BwAuthority/README.BwAuthorities">active bandwidth measurements</a>, so we use the bandwidth weights in the network consensus rather than the weights in each relay descriptor. We know that change <a href="http://metrics.torproject.org/performance.html?graph=torperf&amp;start=2009-08-01&amp;end=2009-10-01#torperf">improved performance</a>, but at what cost to safety?</p>

<p>C) What happens to the network diversity if we <a href="https://trac.torproject.org/projects/tor/ticket/2286">put a low cap</a> on the bandwidth weighting of any node that hasn&#39;t been assigned a measurement by the bandwidth authorities yet, to protect against relays that lie about their bandwidth? If there&#39;s no real effect, we should do it; but if there&#39;s a large effect, we should find a better plan.</p>

<p>D) If we move forward with our plans to <a href="https://trac.torproject.org/projects/tor/ticket/1854">discard all relays under a given bandwidth capacity</a>, how will various choices of bandwidth threshold impact the network&#39;s safety?</p>

<p>E) How about if we discard the X% of paths with the highest expected latency, as <a href="http://swiki.cc.gatech.edu:8080/ugResearch/uploads/7/ImprovingTor.pdf">suggested</a> by Stephen Rollyson?</p>

<p>F) What if some Tor users choose their paths to optimize a different network property (like latency or jitter), as <a href="http://freehaven.net/anonbib/#DBLP:conf/pet/SherrBL09">suggested</a> by Micah Sherr? The infrastructure you&#39;ve built to measure diversity here should apply there too.</p>

<p>G) If only a given fraction of exit relays support IPv6, how much does it reduce your anonymity to be going to an IPv6-only destination?</p>

<p>We&#39;ve put a lot of effort in the past year into understanding <a href="https://blog.torproject.org/blog/why-tor-is-slow">how to improve Tor&#39;s performance</a>, but many of these design changes involve trading off safety for performance, and we still have very little understanding about how much safety Tor offers in the first place. Please help!</p>

<p>If you like Java, check out the <a href="http://metrics.torproject.org/tools.html#metrics-db">metrics database tool</a> (and its <a href="https://gitweb.torproject.org/metrics-db.git/blob_plain/HEAD:/doc/manual.pdf">manual</a>) that Karsten maintains.</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
