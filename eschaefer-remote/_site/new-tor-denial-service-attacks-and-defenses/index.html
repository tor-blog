<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>New Tor Denial of Service Attacks and Defenses</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>New Tor Denial of Service Attacks and Defenses</h2>
<p class="meta">25 Jan 2014</p>

<div class="post">
<p>New work on denial of service in Tor will be presented at <a href="http://www.internetsociety.org/ndss2014/programme#session5">NDSS &#39;14 on Tuesday, February 25th, 2014</a>:</p>

<p><cite><br>
<b>The Sniper Attack: Anonymously Deanonymizing and Disabling the Tor Network</b><br>
by Rob Jansen, Florian Tschorsch, Aaron Johnson, and Björn Scheuermann<br>
To appear at the 21st Symposium on Network and Distributed System Security<br>
</cite></p>

<p><a href="https://www-users.cs.umn.edu/%7Ejansen/publications/sniper-ndss2014.pdf">A copy of the published paper is now available</a>, as are my slides explaining the attacks in both <a href="https://www-users.cs.umn.edu/%7Ejansen/talks/sniper-dcaps-20131011.pptx">PPTX</a> and <a href="https://www-users.cs.umn.edu/%7Ejansen/talks/sniper-dcaps-20131011.pdf">PDF</a> formats.</p>

<p>We found a new vulnerability in the design of Tor&#39;s flow control algorithm that can be exploited to remotely crash Tor relays. The attack is an extremely <strong>low resource</strong> attack in which an adversary&#39;s bandwidth may be traded for a target relay&#39;s memory (RAM) at an amplification rate of one to two orders of magnitude. Ironically, the adversary can use Tor to protect it&#39;s identity while attacking Tor without significantly reducing the effectiveness of the attack.</p>

<p>We studied <strong>relay availability</strong> under the attack using <a href="https://shadow.github.io/">Shadow</a>, a discrete-event network simulator that runs the real Tor software in a safe, private testing environment, and found that we could disable each of the fastest guard and the fastest exit relay in a range of 1-18 minutes (depending on relay RAM capacity). We also found that the entire group of the top 20 exit relays, representing roughly 35% of Tor bandwidth capacity at the time of the analysis, could be disabled in a range of 29 minutes to 3 hours and 50 minutes. We also analyzed how the attack could potentially be used to <strong>deanonymize</strong> hidden services, and found that it would take between 4 and 278 hours before the attack would succeed (again depending on relay RAM capacity, as well as the bandwidth resources used to launch the attack).</p>

<p>Due to our devastating findings, we also designed three defenses that mitigate our attacks, one of which provably renders the attack ineffective. Defenses have been implemented and deployed into the Tor software to ensure that <strong>the Tor network is no longer vulnerable as of Tor version 0.2.4.18-rc and later</strong> . Some of that work can be found in Trac tickets <a href="https://trac.torproject.org/projects/tor/ticket/9063">#9063</a>, <a href="https://trac.torproject.org/projects/tor/ticket/9072">#9072</a>, <a href="https://trac.torproject.org/projects/tor/ticket/9093">#9093</a>, and <a href="https://trac.torproject.org/projects/tor/ticket/10169">#10169</a>.</p>

<p>In the remainder of this post I will detail the attacks and defenses we analyzed, noting again that this information is presented more completely (and more elegantly) in <a href="https://www-users.cs.umn.edu/%7Ejansen/publications/sniper-ndss2014.pdf">our paper</a>.</p>

<hr>

<h1>The Tor Network Infrastructure</h1>

<p>The Tor network is a distributed system made up of thousands of computers running the Tor software that contribute their bandwidth, memory, and computational resources for the greater good. These machines are called Tor <strong>relays</strong> , because their main task is to forward or relay network traffic to another entity after performing some cryptographic operations. When a Tor user wants to download some data using Tor, the user&#39;s Tor client software will choose three relays from those available (an <strong>entry</strong> , <strong>middle</strong> , and <strong>exit</strong> ), form a path or <strong>circuit</strong> between these relays, and then instruct the third relay (the exit) to fetch the data and send it back through the circuit. The data will get transferred from its source to the exit, from the exit to the middle, and from the middle to the entry before finally making its way to the client.</p>

<h1>Flow Control</h1>

<p>The client may request the exit to fetch large amounts of data, and so Tor uses a window-based <a href="http://en.wikipedia.org/wiki/Flow_control_%28data%29">flow control</a> scheme in order to limit the amount of data each relay needs to buffer in memory at once. When a circuit is created, the exit will initialize its circuit <strong>package</strong> counter to 1000 cells, indicating that it is willing to send 1000 cells into the circuit. The exit decrements the package counter by one for every data cell it sends into the circuit (to the middle relay), and stops sending data when the package counter reaches 0. The client at the other end of the circuit keeps a <strong>delivery</strong> counter, and initializes it to 0 upon circuit creation. The client increments the delivery counter by 1 for every data cell it receives on that circuit. When the client&#39;s delivery counter reaches 100, it sends a special Tor control cell, called a SENDME cell, to the exit to signal that it received 100 cells. Upon receiving the SENDME, the exit adds 100 to its package counter and continues sending data into the circuit.</p>

<p>This flow control scheme limits the amount of outstanding data that may be in flight at any time (between the exit and the client) to 1000 cells, or about 500 KiB, per circuit. The same mechanism is used when data is flowing in the opposite direction (up from the client, through the entry and middle, and to the exit).</p>

<h1>The Sniper Attack</h1>

<p>The new Denial of Service (DoS) attack, which we call <strong>&quot;The Sniper Attack&quot;</strong> , exploits the flow control algorithm to remotely crash a victim Tor relay by depleting its memory resources. The paper presents three attacks that rely on the following two techniques:</p>

<ol>
<li>the attacker <strong>stops reading from the TCP connection containing the attack circuit</strong> , which causes the TCP window on the victim&#39;s outgoing connection to close and the victim to buffer up to 1000 cells; and</li>
<li>the attacker causes cells to be continuously sent to the victim (exceeding the 1000 cell limit and consuming the victim&#39;s memory resources) either by <strong>ignoring the package window at packaging end</strong> of the circuit, or by <strong>continuously sending SENDMEs from the delivery end</strong> to the packaging end even though no cells have been read by the delivery end.</li>
</ol>

<p>I&#39;ll outline the attacks here, but remember that the <a href="https://www-users.cs.umn.edu/%7Ejansen/publications/sniper-ndss2014.pdf">paper</a> and <a href="https://www-users.cs.umn.edu/%7Ejansen/talks/sniper-dcaps-20131011.pdf">slides</a> provide more details and useful illustrations of each of the three versions of the attack.</p>

<h2>Basic Version 1 (attacking an entry relay)</h2>

<p>In basic version 1, the adversary controls the client and the exit relay, and chooses a victim for the entry relay position. The adversary builds a circuit through the victim to her own exit, and then the exit continuously generates and sends arbitrary data through the circuit toward the client while ignoring the package window limit. The client stops reading from the TCP connection to the entry relay, and the entry relay buffers all data being sent by the exit relay until it is killed by its OS out-of-memory killer.</p>

<h2>Basic Version 2 (attacking an exit relay)</h2>

<p>In basic version 2, the adversary controls the client and an Internet destination server (e.g. website), and chooses a victim for the exit relay position. The adversary builds a circuit through the victim exit relay, and then the client continuously generates and sends arbitrary data through the circuit toward the exit relay while ignoring the package window limit. The destination server stops reading from the TCP connection to the exit relay, and the exit relay buffers all data being sent by the client until it is killed by its OS out-of-memory killer.</p>

<h2>Efficient Version</h2>

<p>Both of the basic versions of the attack above require the adversary to generate and send data, consuming roughly the same amount of upstream bandwidth as the victim&#39;s available memory. The efficient version reduces this cost by one to two orders of magnitude.</p>

<p>In the efficient version, the adversary controls <strong>only a client</strong> . She creates a circuit, choosing the victim for the entry position, and then instructs the exit relay to download a large file from some external Internet server. The client stops reading on the TCP connection to the entry relay, causing it to buffer 1000 cells.</p>

<p>At this point, the adversary may &quot;trick&quot; the exit relay into sending more cells by sending it a SENDME cell, even though the client has not actually received any cells from the entry. As long as this SENDME does not increase the exit relay&#39;s package counter to greater than 1000 cells, the exit relay will continue to package data from the server and send it into the circuit toward the victim. If the SENDME does cause the exit relay&#39;s package window to exceed the 1000 cell limit, it will stop responding on that circuit. However, the entry and middle node will hold the circuit open until the client issues another command, meaning its resources will not be freed.</p>

<p>The bandwidth cost of the attack after circuit creation is simply the bandwidth cost of occasionally sending a SENDME to the exit. The memory consumption speed depends on the bandwidth and congestion of non-victim circuit relays. We describe how to parallelize the attack using multiple circuits and multiple paths with diverse relays in order to draw upon Tor&#39;s inherent resources. We found that with roughly 50 KiB/s of upstream bandwidth, an attacker could consume the victim&#39;s memory at roughly 1 MiB/s. This is highly dependent on the victim&#39;s bandwidth capabilities: relays that use token buckets to restrict bandwidth usage will of course bound the attack&#39;s consumption rate.</p>

<p>Rather than connecting directly to the victim, the adversary may instead launch the attack through a separate Tor circuit using a second client instance and the &quot;Socks4Proxy&quot; or &quot;Socks5Proxy&quot; option. In this case, she may benefit from the anonymity that Tor itself provides in order to evade detection. We found that there is not a significant increase in bandwidth usage when anonymizing the attack in this way.</p>

<h1>Defenses</h1>

<p>A simple but naive defense against the Sniper Attack is to have the guard node watch its queue length, and if it ever fills to over 1000 cells, kill the circuit. This defense does not prevent the adversary from parallelizing the attack by using multiple circuits (and then consuming 1000 cells on each), which we have shown to be extremely effective.</p>

<p>Another defense, called &quot;authenticated SENDMEs&quot;, tries to protect against receiving a SENDME from a node that didn&#39;t actually receive 100 cells. In this approach, a 1 byte nonce is placed in every 100th cell by the packaging end, and that nonce must be included by the delivery end in the SENDME (otherwise the packaging end rejects the SENDME as inauthentic). As above, this does not protect against the parallel attack. It also doesn&#39;t defend against either of the basic attacks where the adversary controls the packaging end and ignores the SENDMEs anyway.</p>

<p>The best defense, as we suggested to the Tor developers, is to implement a custom, adaptive out-of-memory circuit killer in application space (i.e. inside Tor). The circuit killer is only activated when memory becomes scarce, and then it chooses the circuit with the oldest front-most cell in its circuit queue. This will prevent the Sniper Attack by killing off all of the attack circuits.</p>

<p>With this new defense in place, the next game is for the adversary to try to cause Tor to kill an honest circuit. In order for an adversary to cause an honest circuit to get killed, it must ensure that the front-most cell on its malicious circuit queue is at least slightly &quot;younger&quot; than the oldest cell on any honest queue. We show that the Sniper Attack is impractical with this defense: due to fairness mechanisms in Tor, the adversary must spend an extraordinary amount of bandwidth keeping its cells young — bandwidth that would likely be better served in a more traditional brute-force DoS attack.</p>

<p>Tor has implemented a version of the out-of-memory killer for circuits, and is currently working on <a href="https://trac.torproject.org/projects/tor/ticket/10169">expanding this to channel and connection buffers</a> as well.</p>

<h1>Hidden Service Attack and Countermeasures</h1>

<p>The paper also shows how the Sniper Attack can be used to deanonymize hidden services:</p>

<ol>
<li>run a malicious entry guard relay;</li>
<li>run <a href="http://freehaven.net/anonbib/#oakland2013-trawling">the attack from Oakland 2013</a> to learn the current guard relay of the target hidden service;</li>
<li>run the Sniper Attack on the guard from step 2, knocking it offline and causing the hidden service to choose a new guard;</li>
<li>repeat, until the hidden service chooses the relay from step 1 as its new entry guard.</li>
</ol>

<p>The technique to verify that the hidden service is using a malicious guard in step 4 is the same technique used in step 2.</p>

<p>In the paper, we compute the expected time to succeed in this attack while running malicious relays of various capacities. It takes longer to succeed against relays that have more RAM, since it relies on the Sniper Attack to consume enough RAM to kill the relay (which itself depends on the bandwidth capacity of the victim relay). For the malicious relay bandwidth capacities and honest relay RAM amounts used in their estimate, we found that deanonymization would involve between 18 and 132 Sniper Attacks and take between ~4 and ~278 hours.</p>

<p>This attack becomes much more difficult if the relay is rebooted soon after it crashes, and the attack is ineffective when Tor relays are properly defending against the Sniper Attack (see the &quot;Defenses&quot; section above).</p>

<p>Strategies to defend hidden services in particular go beyond <a href="https://blog.torproject.org/blog/improving-tors-anonymity-changing-guard-parameters">those suggested here</a> to include <strong>entry guard rate-limiting</strong> , where you stop building circuits if you notice that your new guards keep going down (failing closed), and <strong>middle guards</strong> , guard nodes for your guard nodes. Both of these strategies attempt to make it harder to coerce the hidden service into building new circuits or exposing itself to new relays, since that is precisely what is needed for deanonymization.</p>

<h1>Open Problems</h1>

<p>The main defense implemented in Tor will start killing circuits when memory gets low. Currently, Tor uses a configuration option (MaxMemInCellQueues) that allows a relay operator to configure when the circuit-killer should be activated. There is likely not one single value that makes sense here: if it is too high, then relays with lower memory will not be protected; if it is too low, then there may be more false positives resulting in honest circuits being killed. Can Tor determine this setting in an OS-independent way that allows relays to automatically find the right value for MaxMemInCellQueues?</p>

<p>The defenses against the Sniper Attack prevent the adversary from crashing the victim relay, but the adversary may still consume a relay&#39;s bandwidth (and memory resources, to a critical level) at relatively low cost. This means that even though the Sniper Attack can no longer kill a relay, it can still consume a large amount of its bandwidth at a relatively low cost (similar to more traditional bandwidth amplification attacks). More analysis of <strong>general bandwidth consumption attacks and defenses</strong> remains a useful research problem.</p>

<p>Finally, <a href="https://blog.torproject.org/blog/hidden-services-need-some-love">hidden services also need some love</a>. More work is needed to redesign them in a way that does not allow a client to cause the hidden service to choose new relays on demand.</p>

</div>
<p>- robgjansen</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
