<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Deterministic Builds Part Two: Technical Details</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Deterministic Builds Part Two: Technical Details</h2>
<p class="meta">04 Oct 2013</p>

<div class="post">
<p>This is the second post in a two-part series on the build security improvements in the <a href="https://blog.torproject.org/category/tags/tbb-30">Tor Browser Bundle 3.0 release cycle</a>.</p>

<p>The <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise">first post</a> described why such security is necessary. This post is meant to describe the technical details with respect to how such builds are produced.</p>

<p>We achieve our build security through a reproducible build process that enables anyone to produce byte-for-byte identical binaries to the ones we release. Elsewhere on the Internet, this process is varyingly called &quot;deterministic builds&quot;, &quot;reproducible builds&quot;, &quot;idempotent builds&quot;, and probably a few other terms, too.</p>

<p>To produce byte-for-byte identical packages, we use <a href="http://gitian.org/">Gitian</a> to build Tor Browser Bundle 3.0 and above, but that isn&#39;t the only option for achieving reproducible builds. We will first describe how we use Gitian, and then go on to enumerate the individual issues that Gitian solves for us, and that we had to solve ourselves through either wrapper scripts, hacks, build process patches, and (in one esoteric case for Windows) direct binary patching.</p>

<h1>Gitian: What is it?</h1>

<p><a href="http://gitian.org/howto.html">Gitian</a> is a thin wrapper around the Ubuntu virtualization tools written in a combination of Ruby and bash. It was originally developed by Bitcoin developers to ensure the build security and integrity of the Bitcoin software.</p>

<p>Gitian uses Ubuntu&#39;s python-vmbuilder to create a qcow2 base image for an Ubuntu version and architecture combination and a set of git and tarball inputs that you specify in a &#39; <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-firefox.yml">descriptor</a>&#39;, and then proceeds to run a shell script that you provide to build a component inside that controlled environment. This build process produces an output set that includes the compiled result and another &quot;output descriptor&quot; that captures the versions and hashes of all packages present on the machine during compilation.</p>

<p>Gitian requires either Intel VT support (for qemu-kvm), or LXC support, and currently only supports launching Ubuntu build environments from Ubuntu itself.</p>

<h1>Gitian: How Tor Uses It</h1>

<p><a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build">Tor&#39;s use of Gitian</a> is slightly more automated and also slightly different than how Bitcoin uses it.</p>

<p>First of all, because Gitian supports only Ubuntu hosts and targets, we must cross compile everything for Windows and MacOS. Luckily, Mozilla <a href="https://developer.mozilla.org/en-US/docs/Cross_Compile_Mozilla_for_Mingw32">provides support</a> for <a href="http://mingw-w64.sourceforge.net/">MinGW-w64</a> as a &quot;third tier&quot; compiler, and does endeavor to work with the MinGW team to fix issues as they arise.</p>

<p>To our further good fortune, we were able to use a MacOS cross compiler created by <a href="https://github.com/mingwandroid">Ray Donnelly</a> based on a <a href="https://github.com/mingwandroid/toolchain4">fork of &quot;Toolchain4&quot;</a>. We owe Ray a great deal for providing his compilers to the public, and he has been most excellent in terms of helping us through any issues we encountered with them. Ray is also working to <a href="https://github.com/diorcety/crosstool-ng.git">merge his patches</a> into the <a href="http://crosstool-ng.org/">crosstools-ng project</a>, to provide a more seamless build process to create rebuilds of his compilers. As of this writing, we are still using <a href="https://mingw-and-ndk.googlecode.com/files/multiarch-darwin11-cctools127.2-gcc42-5666.3-llvmgcc42-2336.1-Linux-120724.tar.xz">his binaries</a> in combination with the <a href="https://launchpad.net/%7Eflosoft/+archive/cross-apple/+packages">flosoft MacOS10.X SDK</a>.</p>

<p>For each platform, we build the components of Tor Browser Bundle in 3 stages, with one descriptor per stage. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-tor.yml">first descriptor</a> builds Tor and its core dependency libraries (OpenSSL, libevent, and zlib) and produces one output zip file. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-firefox.yml">second descriptor</a> builds Firefox. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-bundle.yml">third descriptor</a> combines the previous two outputs along with our included Firefox addons and localization files to produce the actual localized bundle files.</p>

<p>We <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/tree/HEAD:/gitian/">provide a Makefile and shellscript-based wrapper</a> around Gitian to automate the download and authentication of our source inputs prior to build, and to perform a final step that creates a sha256sums.txt file that lists all of the bundle hashes, and can be signed by any number of detached signatures, one for each builder.</p>

<p>It is important to distribute multiple cryptographic signatures to prevent targeted attacks against stealing a single build signing key (because the signing key itself is of course another single point of failure). Unfortunately, GPG currently lacks support for verifying multiple signatures of the same document. Users must manually copy each detached signature they wish to verify into its proper .asc filename suffix. Eventually, we hope to create a stub installer or wrapper script of some kind to simplify this step, as well as add multi-signature support to the Firefox update process. We are also investigating adding a URL and a hash of the package list the <a href="https://gitweb.torproject.org/torspec.git/blob/master:/dir-spec.txt">Tor Consensus</a>, so that the Tor Consensus document itself authenticates our binary packages.</p>

<p>We do not use the Gitian output descriptors or the Gitian signing tools, because the tools are used to sign Gitian&#39;s output descriptors. We found that Ubuntu&#39;s individual packages (which are listed in the output descriptors) varied too frequently to allow this mechanism to be reproducible for very long. However, we do include a <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/versions">list of input versions and hashes</a> used to produce each bundle in the bundle itself. The format of this versions file is the same that we use as input to download the sources. This means it should remain possible to re-build an arbitrary bundle for verification at a later date, assuming that any later updates to Ubuntu&#39;s toolchain packages do not change the output.</p>

<h1>Gitian: Pain Points</h1>

<p>Gitian is not perfect. In fact, many who have tried our build system have remarked that it is not even close to deterministic (and that for this and other reasons &#39;Reproducible Builds&#39; is a better term). In fact, it seems to experience build failures for quite unpredictible reasons related to bugs in one or more of qemu-kvm/LXC, make, qcow copy-on-write image support. These bugs are often intermittent, and simply restarting the build process often causes things to proceed smoothly. This has made the bugs exceedingly tricky to pinpoint and diagnose.</p>

<p>Gitian&#39;s use of tags (especially signed tags) has some bugs and flaws. For this reason, we verify signatures ourselves after input fetching, and provide gitian only with explicit commit hashes for the input source repositories.</p>

<p>We maintain a list of the most common issues in the <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build">build instructions</a>.</p>

<h1>Remaining Build Reproducibility Issues</h1>

<p>By default, the Gitian VM environment controls the following aspects of the build platform that normally vary and often leak into compiled software: hostname, build path, uname output, toolchain version, and time.</p>

<p>However, Gitian is not enough by itself to magically produce reproducible builds. Beyond what Gitian provides, we had to patch a number of reproducibility issues in Firefox and some of the supporting tools on each platform. These include:</p>

<ol>
<li><strong>Reordering due to inode ordering differences (exposed via Python&#39;s os.walk())</strong></li>
</ol>

<p>Several places in the Firefox build process use python scripts to repackage both compiled library archives and zip files. In particular, they tend to obtain directory listings using os.walk(), which is dependent upon the inode ordering of the filesystem. We fix this by sorting those file lists in the applicable places.</p>

<ol>
<li><strong>LC_ALL localizations alter sorting order</strong></li>
</ol>

<p>Sorting only gets you so far, though, if someone from a different locale is trying to reproduce your build. Differences in your character sets will cause these sort orders to differ. For this reason, we set the LC_ALL environment variable to &#39;C&#39; at the top of our Gitian descriptors.</p>

<ol>
<li><strong>Hostname and other OS info leaks in LXC mode</strong></li>
</ol>

<p>For these cases, we simply patch the pieces of Firefox that include the hostname (primarily for <a href="about:buildconfig">about:buildconfig</a>).</p>

<ol>
<li><strong>Millisecond and below timestamps are not fixed by libfaketime</strong></li>
</ol>

<p>Gitian relies on <a href="http://www.code-wizards.com/projects/libfaketime/">libfaketime</a> to set the clock to a fixed value to deal with embedded timestamps in archives and in the build process. However, in some places, Firefox inserts millisecond timestamps into its supporting libraries as part of an informational structure. We simply zero these fields.</p>

<ol>
<li><strong>FIPS-140 mode generates throwaway signing keys</strong></li>
</ol>

<p>A rather insane subsection of the <a href="https://en.wikipedia.org/wiki/FIPS_140">FIPS-140</a> certification standard requires that you distribute signatures for all of your cryptographic libraries. The Firefox build process meets this requirement by generating a temporary key, using it to sign the libraries, and discarding the private portion of that key. Because there are many other ways to intercept the crypto outside of modifying the actual DLL images, we opted to simply remove these signature files from distribution. There simply is no way to verify code integrity on a running system without both OS and coprocessor assistance. Download package signatures make sense of course, but we handle those another way (as mentioned above).</p>

<ol>
<li><strong>On Windows builds, something mysterious causes 3 bytes to randomly vary<br>
in the binary.</strong></li>
</ol>

<p>Unable to determine the source of this, we just bitstomp the binary and regenerate the PE header checksums using strip... Seems fine so far! ;)</p>

<ol>
<li><strong>umask leaks into LXC mode in some cases</strong></li>
</ol>

<p>We fix this by manually setting umask at the top of our Gitian descriptors. Additionally, we found that we had to reset the permissions inside of tar and zip files, as the umask didn&#39;t affect them on some builds (but not others...)</p>

<ol>
<li><strong>Zip and Tar reordering and attribute issues</strong></li>
</ol>

<p>To aid with this and other issues with reproducibility, we created simple shell wrappers for <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/build-helpers/dzip.sh">zip</a> and <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/build-helpers/dtar.sh">tar</a> to eliminate the sources of non-determinism.</p>

<ol>
<li><strong>Timezone leaks</strong></li>
</ol>

<p>To deal with these, we set TZ=UTC at the top of our descriptors.</p>

<h1>Future Work</h1>

<p>The most common question we&#39;ve been asked about this build process is: What can be done to prevent the adversary from compromising the (substantially weaker) Ubuntu build and packaging processes, and further, what about the <a href="http://cm.bell-labs.com/who/ken/trust.html">Trusting Trust attack</a>?</p>

<p>In terms of eliminating the remaining single points of compromise, the first order of business is to build all of our compilers and toolchain directly from sources via their own Gitian descriptors.</p>

<p>Once this is accomplished, we can begin the process of building identical binaries from multiple different Linux distributions. This would require the adversary to compromise multiple Linux distributions in order to compromise the Tor software distribution.</p>

<p>If we can support reproducible builds through cross compiling from multiple architectures (Intel, ARM, MIPS, PowerPC, etc), this also reduces the likelihood of a Trusting Trust attack surviving unnoticed in the toolchain (because the machine code that injects the payload would have to be pre-compiled and present for all copies of the cross-compiled executable code in a way that is still not visible in the sources).</p>

<p>If those Linux distributions also support reproducible builds of the full build and toolchain environment (both <a href="https://wiki.debian.org/ReproducibleBuilds">Debian</a> and <a href="http://securityblog.redhat.com/2013/09/18/reproducible-builds-for-fedora/">Fedora</a> have started this), we can eliminate Trusting Trust attacks entirely by using <a href="http://www.schneier.com/blog/archives/2006/01/countering_trus.html">Diverse Double Compilation</a> between multiple independent distribution toolchains, and/or assembly audited compilers. In other words, we could use the distributions&#39; deterministic build processes to <a href="https://lwn.net/Articles/555902/">verify</a> that identical build environments are produced through Diverse Double Compilation.</p>

<p>As can be seen, much work remains before the system is fully resistant against all forms of malware injection, but even in their current state, reproducible builds are a huge step forward in software build security. We hope this information helps other software distributors to follow the example set by Bitcoin and Tor.</p>

</div>
<p>- mikeperry</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
