<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Obfsproxy: the next step in the censorship arms race</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Obfsproxy: the next step in the censorship arms race</h2>
<p class="meta">16 Feb 2012</p>

<div class="post">
<p>On Feb 9, Iran <a href="https://blog.torproject.org/blog/iran-partially-blocks-encrypted-network-traffic">started to filter SSL connections</a> on much of their network. Since the Tor protocol uses SSL, that means Tor stopped working too — even Tor with bridges, since bridges use SSL too.</p>

<p>We&#39;ve been quietly developing Obfsproxy, a new tool to make it easier to change how Tor traffic looks on the network. In late 2011 Iran <a href="http://freehaven.net/%7Ekarsten/volatile/top-3-direct-users-relative-2011-12-13.png">moved into the #2 position</a> in global Tor user count, and several important political events are scheduled in Iran this month and next. This situation seemed like a good time to test our new tool while also helping improve Internet freedom around the world.</p>

<p>We started with a &quot;Tor Obfsproxy Browser Bundle&quot; with two test obfsproxy bridges in it, to verify that it worked in-country. Then we got over 300 volunteers running more obfsproxy bridges (even with our <a href="https://www.torproject.org/projects/obfsproxy-instructions.html.en">complex build instructions</a>!), and picked fourteen fast stable trustworthy obfsproxy bridges for an updated bundle which we put out the morning of Feb 11. We spent the weekend fixing usability, stability, and scalability bugs, and put out <a href="https://www.torproject.org/projects/obfsproxy#download">another bundle</a> on Feb 13 with new versions of Vidalia, Tor, and Obfsproxy.</p>

<p>Thousands of people in Iran successfully used the Obfsproxy Bundle over the weekend:</p>

<p><img src="https://blog.torproject.org/files/obfsproxy-usage-2012-02-15-4.png" alt="Obfsproxy users in Iran"></p>

<p>We did some spot-checking and it seems that the new addresses on Feb 14 are mostly different from the new addresses on Feb 13; but I would guess these are mostly returning users with dynamic IP addresses, rather than actually fresh users. More importantly, these people will be thinking about Obfsproxy next time the filter cracks down — and based on current events, that next time won&#39;t be far off. Finally, even though it looks like <a href="https://metrics.torproject.org/users.html?graph=direct-users&amp;start=2012-01-12&amp;end=2012-02-16&amp;country=ir&amp;events=on&amp;dpi=72#direct-users">SSL and Tor are back</a>, I expect Iran will keep throttling SSL traffic as they&#39;ve been doing for months, so the Obfsproxy bundle will still be more fun to use in Iran than the normal Tor bundles.</p>

<p><strong>How does it work?</strong></p>

<p>Deep Packet Inspection (DPI) algorithms classify Internet traffic by protocol. That is, they look at a given traffic flow and decide whether it&#39;s http, ssl, bittorrent, vpn, etc. Governments like Iran, China, and Syria increasingly use these tools (and they often purchase them from Western corporations, but <a href="https://media.torproject.org/video/28c3-4800-en-how_governments_have_tried_to_block_tor_h264.mp4">that&#39;s a different story</a>) to implement their country-wide censorship, either by looking for a given protocol and outright blocking it, or by more subtle mechanisms like squeezing down the bandwidth available to a given protocol to discourage its use.</p>

<p>Obfsproxy&#39;s role is to make it easy for Tor&#39;s traffic flows to look like whatever we like. This way Tor can focus on security and anonymity, and Obfsproxy can focus on appearance. The first thing we decided to try looking like was nothing at all: the &quot;obfs2&quot; module adds an encryption wrapper around Tor&#39;s traffic, using a <a href="https://gitweb.torproject.org/obfsproxy.git/blob/HEAD:/doc/obfs2/protocol-spec.txt">handshake</a> that has no recognizable byte patterns.</p>

<p>It won&#39;t work perfectly. For example, the traffic flows will still have recognizable timing, volume, and packet size characteristics; a good entropy test would show that the handshake looks way more random than most handshakes; and the censors could always press the &quot;only allow protocols my DPI box recognizes&quot; panic button. Each step in this arms race aims to force the censor to a) put more development time and DPI resources into examining flows, and b) risk more false positives, that is, risk blocking innocent users that they didn&#39;t realize they&#39;d be blocking.</p>

<p>This particular new obfuscating layer isn&#39;t the most important feature of Obfsproxy. The best part is that makes it easy to design, deploy, and test other obfuscating layers without messing with the rest of Tor. There&#39;s a lot of research into trying to make traffic flows look like other protocols, so for example we could rewrite the Tor flows as valid http that the DPI engine considers harmless. That problem is harder than it sounds — and it sounds hard. But by making a separate component that only has to worry about how the traffic looks, other researchers can try out different strategies without needing to learn so much about the rest of Tor. This approach will also let us easily <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/180-pluggable-transport.txt">plug in other transports</a> like <a href="https://telex.cc/">Telex</a>, and it will also let <strong><em>other</em></strong> circumvention projects reuse Obfsproxy so they don&#39;t have to reinvent our wheels.</p>

<p><strong>Moving forward</strong></p>

<p>One of the choices we faced was how widely and loudly to mention the bundle. While we think it would be hard and/or risky for attackers to block the Obfsproxy <em>protocol</em>, the bundle included 14 preconfigured bridge addresses, and censors could just plug those addresses into their blacklists. We started the weekend telling people to only tell their close friends, but on Sunday we opted for a broader publicity push inside the activist community for two reasons. First, the new Vidalia release (0.2.17) lets users configure their own obfsproxy bridge addresses, so if the preconfigured addresses get blocked the user can just put in new ones. Second, it became clearer that the blocking would let up in a few days once the immediate political pressure was over, and we decided it was more important to get the word out about Obfsproxy in general so these users will know about it next time.</p>

<p>I should point out that I don&#39;t think they were targeting Tor here. They were targeting popular websites that use SSL, like Gmail and Facebook. Tor was collateral damage because we opted to make Tor traffic look like SSL. That said, we should not forget that we are on their radar: they <a href="https://blog.torproject.org/blog/iran-blocks-tor-tor-releases-same-day-fix">targeted Tor by DPI in September 2011</a>, and the <a href="http://www.nu.nl/internet/2603449/mogelijk-nepsoftware-verspreid-naast-aftappen-gmail.html">Diginotar breakin</a> obtained a fake SSL cert for torproject.org.</p>

<p>The next choice we face is: what other communities should we tell? The bundle works great in China too, where their aggressive censorship has been <a href="https://blog.torproject.org/blog/knock-knock-knockin-bridges-doors">a real hassle</a> for us the past year or so. Some other countries in Asia appear to be newly using DPI to recognize Tor traffic (more on that in an upcoming blog post). We have more development work to do before we can keep up with the China arms race, including teaching obfsproxy bridges to automatically report their addresses to us and teaching our <a href="https://bridges.torproject.org/">bridgedb service</a> to give them out, and we need to answer research questions around <a href="https://blog.torproject.org/blog/strategies-getting-more-bridge-addresses">getting more bridges</a>, <a href="https://blog.torproject.org/blog/bridge-distribution-strategies">giving them out in smarter ways</a>, <a href="https://blog.torproject.org/blog/research-problem-five-ways-test-bridge-reachability">learning when they get blocked</a>, and <a href="https://blog.torproject.org/blog/research-problems-ten-ways-discover-tor-bridges">making it hard for censors to find them</a>. We also need to spread the word carefully, since the arms race is as much about <a href="https://www.torproject.org/press/presskit/2010-09-16-circumvention-features.pdf">not drawing the attention of the censors</a> as it is about the technology. But the Obfsproxy Bundle works out of the box right now in every censoring country we know of, so we shouldn&#39;t be <em>too</em> quiet about it.</p>

<p>And finally, thanks go to George Kadianakis for taking Obfsproxy on as his <a href="http://code.google.com/soc/">Google Summer of Code 2011</a> <a href="https://lists.torproject.org/pipermail/tor-talk/2011-April/020234.html">Project</a>; to Nick Mathewson for mentoring him and getting the Obfsproxy architecture going in the right direction; to Sebastian Hahn for spending all weekend with me fixing bugs and making packages; and to Karsten Loesing, Erinn Clark, Robert Ransom, Runa Sandvik, Nick, George, and the broader Tor community for stepping up over the weekend to help us take it from &quot;early prototype&quot; to &quot;deployed software in use by 5000+ people&quot; in 72 hours.</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
