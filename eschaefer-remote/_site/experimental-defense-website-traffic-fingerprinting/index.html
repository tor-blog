<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Experimental Defense for Website Traffic Fingerprinting</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Experimental Defense for Website Traffic Fingerprinting</h2>
<p class="meta">05 Sep 2011</p>

<div class="post">
<p><strong>Updated 09/05/2011</strong> : Add link to actual <a href="https://gitweb.torproject.org/torbrowser.git/blob/maint-2.2:/src/current-patches/0006-Randomize-HTTP-pipeline-order-and-depth.patch">implementation patch</a> on gitweb</p>

<p>Website fingerprinting is the act of recognizing web traffic through surveillance despite the use of encryption or anonymizing software. The general idea is to leverage the fact that many web sites have specific fixed request patterns and response byte counts that are known beforehand. This information can be used to recognize your web traffic despite attempts at encryption or tunneling. Websites that have an abundance of static content and a fixed request structure tend to be vulnerable to this type of surveillance. Unfortunately, there is enough static content on most websites for this to be the case.</p>

<p><a href="http://guh.nu/projects/ta/safeweb/safeweb.pdf">Early work</a> was quick to determine that simple packet-based encryption schemes (such as wireless and/or VPN encryption) were insufficient to prevent recognition of traffic patterns created by popular websites in the encrypted stream. Later, a <a href="http://research.microsoft.com/pubs/119060/WebAppSideChannel-final.pdf">small-scale study</a> determined that a lot of information could be extracted from HTTPS streams using these same approaches against specific websites.</p>

<p>Despite these early results, whenever researchers tried naively applying these techniques to Tor-like systems, they failed to come up with publishable results (meaning the attack did not work against Tor), due largely to the fixed 512 byte cell size, as well as the multiplexing of Tor client traffic over a single TLS connection.</p>

<p>However, last month, a group of researchers <a href="http://lorre.uni.lu/%7Eandriy/papers/acmccs-wpes11-fingerprinting.pdf">succeeded in performing this attack</a> where the others had failed. Their success hinged largely on their use of a simplified yet well-chosen feature set for training their classifiers. Where other researchers simply dumped packet sizes and timings into their classifiers and unsurprisingly got poor results against Tor traffic, this group extracted the time, quantity and direction of traffic, and discarded irrelevant control information such as TCP ACKs.</p>

<p>While the research methodology of the attack side of their work is particularly exemplary (certainly the most thorough study in website fingerprinting to date), their results are still likely insufficient to deploy against the network and expect to catch people engaging in single visits of forbidden websites. For such a use case, even their relatively low false positive rate is going to cause a lot of issues when deployed against large quantities of traffic. Concurrent use of multiple AJAX-enabled websites and/or other applications will also frustrate an attacker. Even without concurrent activity, their &quot;open-world&quot; experiment (the most realistic scenario for dragnet surveillance of Tor traffic) shows true positive accuracy of around 55%.</p>

<p>However, repeated observations over long periods of time are likely sufficient to develop certainty. It is also possible that extracting actual application data lengths from Tor TLS headers would add additional accuracy.</p>

<p>Hence, this is a rather nasty attack. It is basically a one-ended version of the age-old end-to-end correlation attack (where an adversary attempts to observe both the entrance to the network and the exits to correlate traffic flows). With the website fingerprinting attack, the adversary only needs to observe a single entry (a bridge, a guard node, or a regional firewall) of the network to begin gathering information about users who use that entry point.</p>

<p>However, because the attack is only one-ended, many defenses that would be useless against the full end-to-end attack can be considered. In the countermeasures section of their paper, the researchers point out that a Firefox addon that simply performs background HTTP requests concurrent to normal user activity was enough to foil their classifier.</p>

<p>We disagree with the background fetch approach because it seems that a slightly more sophisticated attack would train a separate classifier to recognize the background cover traffic and then subtract it before attempting to classify the remainder. In the face of this concern, it seems that the background request defense is not worth the additional network load until it can be further studied in detail.</p>

<p>Instead, we are deploying an <a href="https://trac.torproject.org/projects/tor/ticket/3914">experimental defense</a> in <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-5">today&#39;s Tor Browser Bundle release</a> that is specifically designed to reduce the information available for feature extraction without adding overhead. The defense is to enable <a href="https://secure.wikimedia.org/wikipedia/en/wiki/HTTP_pipelining">HTTP pipelining</a>, and to randomize the pipeline size as well as the order of requests. The <a href="https://gitweb.torproject.org/torbrowser.git/blob/maint-2.2:/src/current-patches/0006-Randomize-HTTP-pipeline-order-and-depth.patch">source code to the implementation</a> can be viewed on gitweb.</p>

<p>Since normal, non-randomized pipelining is still off by default to this day in Firefox, we are assuming that the published attack results are against serialized request/response behavior, which provides significantly more feature information to the attacker. In particular, we believe a randomized pipeline will eliminate or reduce the utility of the &#39;Size Marker&#39;, &#39;Number Marker&#39;, &#39;Number of Packets&#39;, and &#39;Occurring Packet Sizes&#39; features on sites that support pipelining, due to the batching of requests and responses of arbitrary sizes. More generally, the randomized pipeline should obscure the request vs response size and request ordering information available to the classifier.</p>

<p>Our hope is that the randomized pipeline defense will therefore increase the duration of observation required to establish certainty that a site is being visited, by lowering the true positive rate and/or raising the false positive rate beyond what the researchers observed.</p>

<p>We do not expect this defense to be foolproof. We create it as a prototype, and request that future research papers do not treat the defense as if it were the final solution against website fingerprinting of Tor traffic. In particular, not all websites support pipelining (in fact, an unknown number may deliberately disable it to reduce load), and even those that do will still leak the initial response size as well as the total response size to the attacker. Pipelining may also be disabled by malicious or simply misconfigured exits.</p>

<p>However, the defense could also be improved. We did not attempt to determine the optimal pipeline size or distribution, and are relying on the research community to tweak these parameters as they evaluate the defense.</p>

<p>We could also take more extreme measures, such as building pipelining support into Tor exit nodes. Perhaps better still, we could deploy an <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/ideas/xxx-using-spdy.txt">HTTP to SPDY translation service at exits</a>. The more efficient request bundling of <a href="http://dev.chromium.org/spdy">SPDY</a> would likely obscure request vs response size information yet further. However, as these translations are potentially fragile as well as labor-intensive to implement and deploy, we are unlikely to take these measures without further feedback from and study by the research community.</p>

<p>Alternatively, or perhaps additionally, defenses could be deployed at the <a href="https://gitweb.torproject.org/obfsproxy.git/blob/HEAD:/doc/protocol-spec.txt">obfsproxy plugin layer</a>. Defenses there would not help against an adversary at the bridge or guard node, but would help against regional firewalls.</p>

<p>We would love to hear feedback from the research community about these approaches, and look forward to hearing more results of future attack and defense work along these and other avenues.</p>

</div>
<p>- mikeperry</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
