<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bittorrent over Tor isn't a good idea</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>Bittorrent over Tor isn't a good idea</h2>
<p class="meta">30 Apr 2010</p>

<div class="post">
<p>An increasing number of people are asking us about the <a href="http://hal.inria.fr/docs/00/47/15/56/PDF/TorBT.pdf">recent paper</a> coming out of Inria in France around Bittorrent and privacy attacks. This post tries to explain the attacks and what they imply.</p>

<p>There are three pieces to the attack (or three separate attacks that build on each other, if you prefer).</p>

<p>The first attack is on people who configure their Bittorrent application to proxy their tracker traffic through Tor. These people are hoping to keep their IP address secret from somebody looking over the list of peers at the tracker. The problem is that several popular Bittorrent clients (the authors call out uTorrent in particular, and I think Vuze does it too) just ignore their socks proxy setting in this case. Choosing to ignore the proxy setting is understandable, since modern tracker designs use the UDP protocol for communication, and socks proxies such as Tor only support the TCP protocol -- so the developers of these applications had a choice between &quot;make it work even when the user sets a proxy that can&#39;t be used&quot; and &quot;make it mysteriously fail and frustrate the user&quot;. The result is that the Bittorrent applications made a different security decision than some of their users expected, and now it&#39;s biting the users.</p>

<p>The attack is actually worse than that: apparently in some cases uTorrent, BitSpirit, and libTorrent simply write your IP address directly into the information they send to the tracker and/or to other peers. Tor is doing its job: Tor is _anonymously_ sending your IP address to the tracker or peer. Nobody knows where you&#39;re sending your IP address from. But that probably isn&#39;t what you wanted your Bittorrent client to send.</p>

<p>That was the first attack. The second attack builds on the first one to go after Bittorrent users that proxy the rest of their Bittorrent traffic over Tor also: it aims to let an attacking peer (as opposed to tracker) identify you. It turns out that the Bittorrent protocol, at least as implemented by these popular Bittorrent applications, picks a random port to listen on, and it tells that random port to the tracker as well as to each peer it interacts with. Because of the first attack above, the tracker learns both your real IP address and also the random port your client chose. So if your uTorrent client picks 50344 as its port, and then anonymously (via Tor) talks to some other peer, that other peer can go to the tracker, look for everybody who published to the tracker listing port 50344 (with high probability there&#39;s only one), and voila, the other peer learns your real IP address. As a bonus, if the Bittorrent peer communications aren&#39;t encrypted, the Tor exit relay you pick can also <a href="https://trac.torproject.org/projects/tor/wiki/TheOnionRouter/TorFAQ#CanexitnodeseavesdroponcommunicationsIsntthatbad">watch the traffic</a> and do the attack.</p>

<p>That&#39;s the second attack. Combined, they present a variety of reasons why running any Bittorrent traffic over Tor isn&#39;t going to get you the privacy that you might want.</p>

<p>So what&#39;s the fix? There are two answers here. The first answer is &quot;don&#39;t run Bittorrent over Tor&quot;. We&#39;ve been saying for years not to run Bittorrent over Tor, because the Tor network <a href="https://blog.torproject.org/blog/why-tor-is-slow">can&#39;t handle the load</a>; perhaps these attacks will convince more people to listen. The second answer is that if you want your Bittorrent client to actually provide privacy when using a proxy, you need to get the application and protocol developers to fix their applications and protocols. <a href="https://www.torproject.org/download.html.en#Warning">Tor can&#39;t keep you safe if your applications leak your identity.</a></p>

<p>The third attack from their paper is where things get interesting. For efficiency, Tor <a href="https://svn.torproject.org/svn/projects/design-paper/tor-design.html#sec:intro">puts multiple application streams over each circuit</a>. This approach improves efficiency because we don&#39;t have to waste time and overhead making a new circuit for every tiny picture on the aol.com frontpage, and it improves anonymity because every time you build a new path through the Tor network, you increase the odds that one of the paths you&#39;ve built is observable by an attacker. But the downside is that exit relays can build short snapshots of user profiles based on all the streams they see coming out of a given circuit. If one of those streams identifies the user, the exit relay knows that the rest of those streams belong to that user too.</p>

<p>The result? If you&#39;re using Bittorrent over Tor, and you&#39;re _also_ browsing the web over Tor at the same time, then the above attacks allow an attacking exit relay to break the anonymity of some of your web traffic.</p>

<p>What&#39;s the fix? The same two fixes as before: don&#39;t run Bittorrent over Tor, and/or get your Bittorrent developers to fix their applications.</p>

<p>But as Tor developers, this attack opens up an opportunity for a third fix. Is there a way that we as Tor can reduce the damage that users can do to themselves when they use insecure applications over Tor? We can&#39;t solve the fact that you&#39;ll shoot yourself in the foot if you use Bittorrent over Tor, but maybe we can still save the rest of the leg.</p>

<p>One approach to addressing this problem in Tor&#39;s design is to make each user application use a separate circuit. In Linux and Unix, we can probably hack something like that up -- there are ways to look up the pid (process ID) of the application connecting to our socket. I suspect it gets harder in Windows though. It also gets harder because many Tor applications use an intermediate http proxy, like Polipo or Privoxy, and we&#39;d have to teach these other proxies how to distinguish between different applications and then pass that information along to Tor.</p>

<p>Another answer is to separate streams by destination port. Then all the streams that go to port 80 are on one circuit, and a stream for a different destination port goes on another circuit. We&#39;ve had that idea lurking in the background for a long time now, but it&#39;s actually because of Bittorrent that we haven&#39;t implemented it: if a BT client asks us to make 50 streams to 50 different destination ports, I don&#39;t want the Tor client to try to make 50 different circuits. That puts too much load on the network. I guess we could special-case it by separating &quot;80&quot; and &quot;not 80&quot;, but I&#39;m not sure how effective that would be in practice, first since many other ports (IM, SSH, etc) would want to be special-cased, and second since firewalls are pressuring more and more of the Internet to go over port 80 these days.</p>

<p>We should keep brainstorming about ways to protect users even when their applications are handing over their sensitive information. But in the mean time, I think it&#39;s great that these researchers are publishing their results and letting everybody else evaluate the attacks. (If you&#39;re a researcher working on Tor attacks or defenses, check out our new <a href="https://www.torproject.org/research.html.en">research resources</a> page.) The attacks in this paper are serious attacks if you&#39;re a Bittorrent user and you&#39;re hoping to have some privacy.</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
