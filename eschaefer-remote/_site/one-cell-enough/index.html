<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>"One cell is enough to break Tor's anonymity"</title>
        <meta name="viewport" content="width=device-width">

        <!-- syntax highlighting CSS -->
        <link rel="stylesheet" href="/css/syntax.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="/css/main.css">

    </head>
    <body>

        <div class="site">
          <div class="header">
            <h1 class="title"><a href="/">Your New Jekyll Site</a></h1>
            <a class="extra" href="/">home</a>
          </div>

          <h2>"One cell is enough to break Tor's anonymity"</h2>
<p class="meta">19 Feb 2009</p>

<div class="post">
<p>Tomorrow there&#39;s a talk at Black Hat DC by Xinwen Fu on an active attack that can allow traffic confirmation in Tor. He calls it a &quot; <a href="http://www.cs.uml.edu/%7Exinwenfu/paper/ICC08_Fu.pdf">replay attack</a>&quot;, whereas we called it a &quot;tagging attack&quot; in the original Tor paper, but let&#39;s look at how it actually works.</p>

<p>First, remember the basics of how Tor provides anonymity. Tor clients <a href="https://www.torproject.org/images/htw2.png">route their traffic</a> over several ( <a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#VariablePathLength">usually three</a>) relays, with the goal that no single relay gets to learn both where the user is (call her Alice) and what site she&#39;s reaching (call it Bob).</p>

<p>The Tor design <a href="https://www.torproject.org/svn/trunk/doc/design-paper/tor-design.html#subsec:threat-model">doesn&#39;t try to protect</a> against an attacker who can see or measure both traffic going into the Tor network and also traffic coming out of the Tor network. That&#39;s because if you can see both flows, some <a href="http://freehaven.net/anonbib/#danezis:pet2004">simple statistics</a> let you decide whether they match up.</p>

<p>Because we aim to let people browse the web, we can&#39;t afford the extra overhead and hours of additional delay that are used in high-latency mix networks like <a href="http://freehaven.net/anonbib/#mixmaster-spec">Mixmaster</a> or <a href="http://freehaven.net/anonbib/#minion-design">Mixminion</a> to slow this attack. That&#39;s why Tor&#39;s security is all about trying to decrease the chances that an adversary will end up in the right positions to see the traffic flows.</p>

<p>The way we generally explain it is that Tor tries to protect against traffic analysis, where an attacker tries to learn whom to investigate, but Tor can&#39;t protect against traffic confirmation (also known as end-to-end correlation), where an attacker tries to confirm a hypothesis by monitoring the right locations in the network and then doing the math.</p>

<p>And the math is really effective. There are simple packet counting attacks ( <a href="http://freehaven.net/anonbib/#SS03">Passive Attack Analysis for Connection-Based Anonymity Systems</a>) and moving window averages ( <a href="http://freehaven.net/anonbib/#timing-fc2004">Timing Attacks in Low-Latency Mix-Based Systems</a>), but the more recent stuff is <a href="http://www.lightbluetouchpaper.org/2007/05/28/sampled-traffic-analysis-by-internet-exchange-level-adversaries/">downright scary</a>, like Steven Murdoch&#39;s PET 2007 paper about achieving high confidence in a correlation attack despite seeing only 1 in 2000 packets on each side ( <a href="http://freehaven.net/anonbib/#murdoch-pet2007">Sampled Traffic Analysis by Internet-Exchange-Level Adversaries</a>).</p>

<p>What Fu is presenting in his talk is another instance of the confirmation attack, called the tagging attack. The basic idea is that an adversary who controls both the first (entry) and last (exit) relay that Alice picks can modify the data flow at one end of the circuit (&quot;tag&quot; it), and detect that modification at the other end — thus bridging the circuit and confirming that it really is Alice talking to Bob. This attack has some limitations compared to the above attacks. First, it involves modifying data, which in most cases will break the connection; so there&#39;s a lot more risk that he&#39;ll be noticed. Second, the attack relies on the adversary actually controlling both relays. The passive variants can be performed by an observer like an ISP or a telco.</p>

<p>Tagging attacks on designs like Tor go back over a decade in the literature. In the 1996 Onion Routing paper ( <a href="http://freehaven.net/anonbib/#onion-routing:ih96">Hiding Routing Information</a>), they wrote:</p>

<blockquote>
<p>If the responder&#39;s proxy [exit node] is compromised, and can determine when the unencrypted data stream has been corrupted, it is possible for compromised nodes earlier in the virtual circuit to corrupt the stream and ask which responder&#39;s proxy received uncorrupted data. By working with compromised nodes around a suspected initiator&#39;s proxy [Tor client], one can identify the beginning of the virtual circuit.</p>
</blockquote>

<p>When we designed Tor, we made a conscious decision to not design against this attack, because the other confirmation attacks are just as effective and we have no fix for them, and because countering the tagging attack doesn&#39;t come for free. Here&#39;s a quote from <a href="https://www.torproject.org/svn/trunk/doc/design-paper/tor-design.html#subsec:integrity-checking">the Tor design paper</a> in 2004:</p>

<blockquote>
<p>Because Tor uses TLS on its links, external adversaries cannot modify data. Addressing the insider malleability attack, however, is more complex.</p>

<p>We could do integrity checking of the relay cells at each hop, either by including hashes or by using an authenticating cipher mode like EAX, but there are some problems. First, these approaches impose a message-expansion overhead at each hop, and so we would have to either leak the path length or waste bytes by padding to a maximum path length. Second, these solutions can only verify traffic coming from Alice: ORs would not be able to produce suitable hashes for the intermediate hops, since the ORs on a circuit do not know the other ORs&#39; session keys. Third, we have already accepted that our design is vulnerable to end-to-end timing attacks; so tagging attacks performed within the circuit provide no additional information to the attacker. Thus, we check integrity only at the edges of each stream.</p>
</blockquote>

<p>Basically, we chose a design where the tagging attack is no more effective than the passive confirmation attacks, and figured that would have to be good enough.</p>

<p>I should also note here that the correlation attack papers mostly focus on _observing_ flows at either end and matching them up. They do great with just that. But think what you could do if you actually control one of the endpoints, and can modulate how much you send, when you send it, etc. Heck, if you&#39;re the exit relay, you can spoof a much larger webpage for the user, and then have even more bytes to work with. None of that requires a tagging attack.</p>

<p>So, where does the &quot;one cell is enough&quot; come in? If you can tag a single cell and recognize it on the other end of the circuit, then you can be confident you&#39;ve got the right flow.</p>

<p>One of the unknowns in the research world is exactly how quickly the timing attack succeeds. How many seconds of traffic (and/or packets) do you need to achieve a certain level of confidence? I&#39;ll grant that if you run the entry and exit, tagging is a very simple attack to carry out both conceptually and in practice. But I think Fu underestimates how simple the timing attack can be also. That&#39;s probably the fundamental disagreement here.</p>

<p>For example, Bauer et al. in WPES 2007 described a passive timing attack on Tor circuit creation that works before a single relay cell has been transmitted ( <a href="http://freehaven.net/anonbib/#bauer:wpes2007">Low-Resource Routing Attacks Against Tor</a>).</p>

<p>Fu&#39;s paper also cites concern about false positives -- it seems on first glance that a tagging attack should provide much higher confidence than a timing attack, since you&#39;ll always be wondering whether the timing attack really matched up the right circuits. Here&#39;s a quote from one of the authors of <a href="http://freehaven.net/anonbib/#hs-attack06">Locating Hidden Servers</a>, another paper with a successful timing attack on Tor:</p>

<blockquote>
<p>We were prepared to do parameter tuning, active timing signature insertion, etc. should it be necessary, but it wasn&#39;t. In the thousands of circuits we ran we _never_ had a false positive. The Bauer et al. paper from WPES&#39;07 extended our work from hidden server circuits to general Tor circuits (although it only ran on a Tor testbed on PlanetLab rather than the public Tor network). The highest false positive rate they got was .0006. This is just a nonissue.</p>
</blockquote>

<p>One of the challenges here is that anonymity designs live on the edge. While crypto algorithms aim to be so good that even really powerful attackers still can&#39;t succeed, anonymity networks are constantly balancing performance and efficiency against attacks like these. One of the tradeoffs we made in the Tor design is that we accepted end-to-end correlation as an attack that is too expensive to solve, and we&#39;re building the best design we can based on that.</p>

<p>If somebody can show us that tagging attacks are actually much more effective than their passive timing counterparts, we should work harder to fix them. If somebody can come up with a cheap way to make them harder, we&#39;re all ears. But while they remain on par with passive attacks and remain expensive to fix, then it doesn&#39;t seem like a good move to slow down Tor even more without actually making it safer.</p>

<p>Overall, I&#39;m confused why the conference organizers thought this would be a good topic for a Black Hat talk. It&#39;s not like there&#39;s a shortage of &quot;oh crap&quot; Tor attack papers lately. I guess my take-away message is that I need to introduce Jeff Moss to Nick Hopper et al from Minnesota ( <a href="http://freehaven.net/anonbib/#tissec-latency-leak">How Much Anonymity does Network Latency Leak?</a>) and Sambuddho Chakravarty et al from Columbia ( <a href="http://www.google.com/search?q=columbia+global+passive+adversary+tor">Approximating a Global Passive Adversary against Tor</a>).</p>

</div>
<p>- arma</p>


          <div class="footer">
            <div class="contact">
              <p>
                Your Name<br />
                What You Are<br />
                you@example.com
              </p>
            </div>
            <div class="contact">
              <p>
                <a href="https://github.com/yourusername">github.com/yourusername</a><br />
                <a href="https://twitter.com/yourusername">twitter.com/yourusername</a><br />
              </p>
            </div>
          </div>
        </div>

    </body>
</html>
