---
layout: post
title: "September 2011 Progress Report"
permalink: september-2011-progress-report
date: 2011-10-09
author: phobos
category: blog
tags: ["iran", "progress report", "protocol enhancements"]
---

In September 2011 we made progress in a number of areas, such as handling issues in Iran's use of DPI to block tor, new versions of Tor, Tails 0.8 release, and more.

The PDF and plaintext versions of the report can be found attached to this blog post or at our monthly report archive:

[https://archive.torproject.org/monthly-report-archive/2011-September-Mon...](https://archive.torproject.org/monthly-report-archive/2011-September-Monthly-Report.pdf "https://archive.torproject.org/monthly-report-archive/2011-September-Monthly-Report.pdf")

and

[https://archive.torproject.org/monthly-report-archive/2011-September-Mon...](https://archive.torproject.org/monthly-report-archive/2011-September-Monthly-Report.txt "https://archive.torproject.org/monthly-report-archive/2011-September-Monthly-Report.txt")

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2011-September-Monthly-Report.pdf">2011-September-Monthly-Report.pdf</a></td>
<td>672.87 KB</td> </tr>
 <tr class="even">
<td><a href="https://blog.torproject.org/files/2011-September-Monthly-Report.txt">2011-September-Monthly-Report.txt</a></td>
<td>35.72 KB</td> </tr>
</tbody>

