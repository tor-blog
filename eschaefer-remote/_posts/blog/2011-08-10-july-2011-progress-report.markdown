---
layout: post
title: "July 2011 Progress Report"
permalink: july-2011-progress-report
date: 2011-08-10
author: phobos
category: blog
tags: ["advocacy", "censorship circumvention", "progress report", "scalability", "translations"]
---

The July 2011 Progress Report is at the bottom of this post and at [https://blog.torproject.org/files/2011-July-Monthly-Report.pdf](https://blog.torproject.org/files/2011-July-Monthly-Report.pdf "https://blog.torproject.org/files/2011-July-Monthly-Report.pdf").

Highlights include continued progress on protocol obfuscating proxy, a new bridge guard design, outreach, scalability improvements, orbot updates, and a number of translation updates.

Update 2011-08-15: based on feedback, created a plaintext version of the pdf. It doesn't contain the images obviously, but does contain all of the content. Generated the text file via pandoc. The text file is here, [https://blog.torproject.org/files/2011-July-Monthly-Report.txt](https://blog.torproject.org/files/2011-July-Monthly-Report.txt "https://blog.torproject.org/files/2011-July-Monthly-Report.txt")

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2011-July-Monthly-Report.pdf">2011-July-Monthly-Report.pdf</a></td>
<td>670.75 KB</td> </tr>
 <tr class="even">
<td><a href="https://blog.torproject.org/files/2011-July-Monthly-Report.txt">2011-July-Monthly-Report.txt</a></td>
<td>23.1 KB</td> </tr>
</tbody>

