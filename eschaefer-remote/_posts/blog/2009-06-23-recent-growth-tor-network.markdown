---
layout: post
title: "On the Recent Growth of the Tor Network"
permalink: recent-growth-tor-network
date: 2009-06-23
author: karsten
category: blog
tags: ["bridges", "metrics", "performance"]
---

In the past few days the Tor network is seeing a lot of new users [coming from Iran](https://blog.torproject.org/blog/measuring-tor-and-iran). At the same time we have heard from many people who want to support the Tor network by setting up more relays and [bridges](https://www.torproject.org/bridges). Now we wanted to know, are these just promises, or did the network really grow? Here are the results:

The number of relays has grown by 359 or 24% to now 1827 relays within only one week to the highest [number of relays](https://git.torproject.org/checkout/metrics/master/report/dirarch/dirarch-2009-06-22.pdf) the network has ever seen. Likewise, the [number of bridges](https://git.torproject.org/checkout/metrics/master/report/bridges/bridges-2009-06-22.pdf) has increased by an incredible 69 % from 255 to 432 within one week. Awesome!

<center><img height="350" width="500" src="https://blog.torproject.org/files/relays-2009-06-21.png"></center>

<center><img height="350" width="500" src="https://blog.torproject.org/files/bridges-2009-06-21.png"></center>

So, does this mean everything is taken care of and no more relays or bridges are needed? Not at all! As you may know, Tor has some [performance issues](https://blog.torproject.org/blog/why-tor-is-slow) that are, among other things, the result of too few bandwidth capacity for too many clients. If you can, go [setup more relays and/or bridges](https://www.torproject.org/docs/tor-doc-relay) and keep them running even when the conflicts in the world do not hit the headlines. Be sure that your support is much appreciated!

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/relays-2009-06-21.png">relays-2009-06-21.png</a></td>
<td>15.28 KB</td> </tr>
 <tr class="even">
<td><a href="https://blog.torproject.org/files/bridges-2009-06-21.png">bridges-2009-06-21.png</a></td>
<td>15.02 KB</td> </tr>
</tbody>

