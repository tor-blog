---
layout: post
title: "Academic (Postdoc, PhD, and Master's) positions available for Tor-related research"
permalink: academic-postdoc-phd-and-masters-positions-available-tor-related-research
date: 2011-09-24
author: ian
category: blog
tags: ["academia", "research"]
---

As more anonymous communications and PETs researchers join the academic ranks, there is an increasing availability of Tor-related research positions at the postdoctoral, PhD, and Master's levels. (Note: we prefer the term "training of Highly Qualified Personnel" to "pyramid scheme".)

Steven Murdoch (at Cambridge) and I (at Waterloo) each have some such positions available. See information and links below.

- Ian

<dl>
<dt><strong>PhD position at Cambridge</strong></dt>
<dd>Funding is available for a PhD student to work at the University of Cambridge Computer Laboratory, on the topic of privacy enhancing technologies and anonymous communications, starting in April 2012.
<p>The sponsorship is jointly provided by Microsoft Research Cambridge and under the Dorothy Hodgkin Postgraduate Awards scheme. As such, applicants must be nationals from India, China, Hong Kong, South Africa, Brazil, Russia or <a href="http://www.oecd.org/dataoecd/32/40/43540882.pdf" rel="nofollow">countries in the developing world</a> as defined by the Development Assistance Committee of the OECD.</p>
<p>The application deadline is soon (28 October 2011), so please circulate this advertisement to anyone who you think might find it of interest.</p>
<p>Further details can be found on the <a href="http://www.admin.cam.ac.uk/offices/hr/jobs/vacancies.cgi?job=8848" rel="nofollow">University website</a>, and enquiries should be sent to <a href="mailto:Steven.Murdoch@cl.cam.ac.uk" rel="nofollow">&lt;Steven.Murdoch@cl.cam.ac.uk&gt;</a>.</p>
</dd>
<dt><strong>PhD and Master's positions at Waterloo</strong></dt>
<dd>One of the particular focuses of the Cryptography, Security, and Privacy (CrySP) group at the University of Waterloo is research into privacy-enhancing technologies and anonymous communication networks. We have much ongoing work on Tor, with an eye to improving its security, privacy, scalability, and performance.
<p>Students interested in applying for PhD and Master's research positions in the CrySP group should email Ian Goldberg <a href="mailto:iang@cs.uwaterloo.ca" rel="nofollow">&lt;iang@cs.uwaterloo.ca&gt;</a> with "Tor research position" in the subject line.</p>
</dd>
<dt><strong>Postdoc position at Waterloo</strong></dt>
<dd>The Cryptography, Security, and Privacy (CrySP) research group at the University of Waterloo is seeking applications for a postdoctoral research position in the field of privacy-enhancing technologies, preferably on the topic of privacy-preserving communications systems. This position will be held in the Cheriton School of Computer Science.
<p>Applicants must hold a PhD in a related field, and should have a proven research record, as demonstrated by publications in top security and privacy venues (such as Oakland, CCS, USENIX Security, and NDSS) and/or top venues specific to privacy-enhancing technologies (such as PETS).</p>
<p>The start date of the position is negotiable. The position may be for one or two years.</p>
<p>Applicants should submit a CV, a research plan, two or three selected papers, and the names and contact information of three references.</p>
<p>For further information about the position, or to apply, please send email to Ian Goldberg <a href="mailto:iang@cs.uwaterloo.ca" rel="nofollow">&lt;iang@cs.uwaterloo.ca&gt;</a> with "Postdoctoral position" in the subject line. Applications may be considered as they arrive.</p>
<p>See <a href="http://crysp.uwaterloo.ca/prospective/postdoc/" rel="nofollow">http://crysp.uwaterloo.ca/prospective/postdoc/</a> for more information.</p>
</dd>
</dl>
