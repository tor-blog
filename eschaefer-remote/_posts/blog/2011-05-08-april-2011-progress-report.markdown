---
layout: post
title: "April 2011 Progress Report"
permalink: april-2011-progress-report
date: 2011-05-08
author: phobos
category: blog
tags: ["anonymity fixes", "arm", "bugfixes", "censorship circumvention", "enhancements", "libevent", "new features", "progress report", "tor releases", "vidalia releases"]
---

The April 2011 Progress Report is attached to this post and available at [https://blog.torproject.org/files/2011-April-Monthly-Report.pdf](https://blog.torproject.org/files/2011-April-Monthly-Report.pdf "https://blog.torproject.org/files/2011-April-Monthly-Report.pdf").

Highlights include releases for tor, vidalia, arm, and libevent. Updates to pluggable transports, obfsproxy, torbutton, many translation and core architecture updates.

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2011-April-Monthly-Report.pdf">2011-April-Monthly-Report.pdf</a></td>
<td>693.16 KB</td> </tr>
</tbody>

