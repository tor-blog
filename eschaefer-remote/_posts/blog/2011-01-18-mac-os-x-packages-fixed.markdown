---
layout: post
title: "Mac OS X packages fixed"
permalink: mac-os-x-packages-fixed
date: 2011-01-18
author: erinn
category: blog
tags: ["apple osx love", "updated packages"]
---

The [packaging bug](https://trac.torproject.org/projects/tor/ticket/2406) has been fixed in the OS X packages and all users can now upgrade. Packages are available on the [download](https://www.torproject.org/download) page.

