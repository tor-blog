---
layout: post
title: "How to Circumvent an Internet Proxy - Howcast"
permalink: how-circumvent-internet-proxy-howcast
date: 2008-12-06
author: phobos
category: blog
tags: ["censorship circumvention", "howcast", "video"]
---

Howcast produced a quick video for the masses on [how to circumvent censorship](http://www.howcast.com/videos/90601-How-To-Circumvent-an-Internet-Proxy). We were technical consultants for this video. It's tough to talk about Tor, when the first question you're trying to answer is "What is a proxy? And why do I care?"

Howcast did a great job for a high-level overview of circumvention technologies in four minutes.

<object width="432" height="276" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="howcastplayer"><param name="movie" value="http://www.howcast.com/flash/howcast_player.swf?file=90601&amp;theme=black">
<param name="allowFullScreen" value="false">
<param name="allowScriptAccess" value="always">
<embed src="http://www.howcast.com/flash/howcast_player.swf?file=90601&amp;theme=black" type="application/x-shockwave-flash" width="432" height="276" allowfullscreen="false" allowscriptaccess="always"></embed></object>
