---
layout: post
title: "New packages with OpenSSL updates"
permalink: new-packages-openssl-updates
date: 2012-01-05
author: erinn
category: blog
tags: ["package updates", "tbb", "tor browser bundle"]
---

The Tor Browser Bundles and other packages have been updated to [OpenSSL 1.0.0f and 0.9.8s](http://openssl.org/news/secadv_20120104.txt). All users are encouraged to update.

[https://www.torproject.org/download](https://www.torproject.org/download "https://www.torproject.org/download")

**Tor Browser Bundle (2.2.35-4)**

- Update OpenSSL to 1.0.0f
- Update NoScript to 2.2.5

