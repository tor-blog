---
layout: post
title: "Update on Tor usage in Tunisia"
permalink: update-tor-usage-tunisia
date: 2011-01-14
author: phobos
category: blog
tags: ["cautious optimism", "internet censorship", "tunisia", "users"]
---

It has been reported that the Tunisian President has [stopped the Internet censorship](http://english.aljazeera.net/news/africa/2011/01/2011113192110570350.html) in the country. We have confirmation from a few Tunisians that Tor works from within the country, without requiring bridges. And that Youtube and some other sites are no longer censored. There is cautious optimism that this is real, however many continue to use Tor to protect their Internet traffic as a safeguard.

Two graphs show the current state of Tor usage in Tunisia:

![](https://blog.torproject.org/files/bridge-users-2011-01-14-tn-2010-10-16.png)

![](https://blog.torproject.org/files/direct-users-2011-01-14-tn-2010-10-16.png)

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/bridge-users-2011-01-14-tn-2010-10-16.png">bridge-users-2011-01-14-tn-2010-10-16.png</a></td>
<td>9.28 KB</td> </tr>
 <tr class="even">
<td><a href="https://blog.torproject.org/files/direct-users-2011-01-14-tn-2010-10-16.png">direct-users-2011-01-14-tn-2010-10-16.png</a></td>
<td>13.45 KB</td> </tr>
</tbody>

