---
layout: post
title: "New Stable Tor Browser Bundles"
permalink: new-stable-tor-browser-bundles
date: 2012-08-17
author: erinn
category: blog
tags: ["tbb", "tor", "tor browser", "tor stable"]
---

The stable Tor Browser Bundles have all been updated to the latest Tor 0.2.2.38 stable release.

[https://www.torproject.org/download](https://www.torproject.org/download "https://www.torproject.org/download")

**Tor Browser Bundle (2.2.38-1)**

- Update Tor to 0.2.2.38
- Update NoScript to 2.5
- Update HTTPS Everywhere to 2.1

