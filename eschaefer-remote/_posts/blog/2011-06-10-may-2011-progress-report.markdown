---
layout: post
title: "May 2011 Progress Report"
permalink: may-2011-progress-report
date: 2011-06-10
author: phobos
category: blog
tags: ["anonymity advocacy", "bugfixes", "enhancements", "progress report"]
---

The May 2011 progress report is available at the bottom of this post and at [https://blog.torproject.org/files/2011-May-Monthly-Report.pdf](https://blog.torproject.org/files/2011-May-Monthly-Report.pdf "https://blog.torproject.org/files/2011-May-Monthly-Report.pdf").

Highlights include an experimental tor release, experimental vidalia release, some timing attack resistance work, some ECC work, updates on obfsproxy, and a datagram protocol comparison.

<thead><tr>
<th>Attachment</th>
<th>Size</th> </tr></thead><tbody>
 <tr class="odd">
<td><a href="https://blog.torproject.org/files/2011-May-Monthly-Report.pdf">2011-May-Monthly-Report.pdf</a></td>
<td>688.86 KB</td> </tr>
</tbody>

